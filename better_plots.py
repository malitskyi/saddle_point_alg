import matplotlib as mpl
import matplotlib.pyplot as plt

def set_matplotlib():
    mpl.rc('lines', linewidth=2)
    mpl.rcParams.update(
        {'font.size': 12, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})
    mpl.rcParams['xtick.major.pad'] = 2
    mpl.rcParams['ytick.major.pad'] = 2
    
