import numpy as np
import scipy.linalg as LA
from sklearn.feature_extraction.image import extract_patches
from scipy.signal import fftconvolve
from scipy import ndimage

def make_v_dict(v, lvl):
    """ Make a dictionary of a given image on different resolutions.

    Parameters
    ----------
    v: 2d array.
    lvl: positive integer
       Number of levels.

    Returns
    -------
    dict with keys 1,..., lvl. 
        dict[i] represents v in the dual format on the i-th level.

    """
    v_dict = {}
    for i in range(1, lvl+1):
        kernel = np.ones((i,i))
        v_dict[i] = transform_x_into_y(v, kernel)
        
    return v_dict

def dot_object(u1, u2, w=None):
    """ Compute the weighted dot product of objects u1 and u2
    
    Parameters
    ----------
    u1, u2: 1d object array of the same shape.
    w: 1d array of the same length as u1
     Represents positive weights.

    Returns
    -------
    A nonnegative number which is a weighted dot product
    w[0] <u1[0], u2[0]> + ... + w[m] <u1[m], u2[m]>.

    """
    p = len(u1)
    s = 0
    if w is None:
        w = np.ones(p)
    for i in range(p):
        s += w[i] * np.vdot(u1[i], u2[i])
    return s

def norm_square_object(u, w=None):
    """ Compute the weighted norm of u.

    Uses that |u|^2_w = w[0] <u[0], u[0]> + ... + w[m] <u[m], u[m]>.

    Parameters
    ----------
    u: 1d object array.
    w: 1d array of the same length as u
     Represents positive weights.

    Returns
    -------
    A nonnegative number which is a weighted norm of u.

    """
    return dot_object(u, u, w)



def check_feasibility(x, v, gamma, lvl, psf=None):
    """Computes how much x violates constraints for SMRE problem.

    For every square s we compute |sum_{i,j \in s} (Ax - v)_ij | -
    gamma_s.  Since we have a sign "<=", we want to have these values
    negative.

    x : 2d array.
        Image that we want to check.

    v : 2d array.
        Given image (noisy or convolved).
    gamma: dictionary with keys 1, 2, lvl
        Stores different noise parameters for each resolution level.
    psf: 2d array or None.
        Kernel for a convolution.
   
    
    Returns
    -------
    1-d array of length N**2 + (N - 1)**2 +...+ (N - lvl + 1)**2.

    """
    N = x.shape[0]
    p = np.sum(np.arange(N - lvl + 1, N+1)**2)
    arr = np.zeros(p)
    if psf is not None:
        x = convolution(x, psf)
    res = x - v
    ind = 0
    for i in range(1, lvl + 1):
        # i-th level
        m = (N - i + 1)**2
        kernel = np.ones((i,i))
        arr[ind:ind+m] = (np.abs(transform_x_into_y(res, kernel)) - gamma[i]).ravel()  # h_s(x)
        ind += m
    return arr

# def check_feasibility_new(x, v, gamma, lvl, psf=None):
#     """ Computes how much x violates constraints for SMRE problem.

#     For every square s we compute |sum_{i,j \in s} (Ax - v)_ij | - gamma_s.
#     Since we have a sign "<=", it is good when this value is negative.

#     Returns
#     -------
#     1-d array of length N**2 + (N-1)**2 +...+ (N-lvl+1)**2.

#     """
#     N = x.shape[0]
#     p = np.sum(np.arange(N - lvl + 1, N+1)**2)
#     arr = np.zeros(p)
#     if psf is not None:
#         x = convolution(x, psf)
#     res = x - v
#     ind = 0
#     for i in range(1, lvl + 1):
#         # i-th level
#         m = (N - i + 1)**2
#         kernel = np.ones((i,i))
#         x_into_y = transform_x_into_y(res, kernel)
#         arr[ind:ind+m] = (np.abs(x_into_y) - gamma[i]).ravel()
#         ind += m
#     return arr



# def size_mb(x):
#   return (x.nbytes/(2**20))

#------------------------------------------------------------------
#------------------------------------------------------------------
#------------------------------------------------------------------
def do_profile(follow=[]):
        def inner(func):
            def profiled_func(*args, **kwargs):
                try:
                    profiler = LineProfiler()
                    profiler.add_function(func)
                    for f in follow:
                        profiler.add_function(f)
                    profiler.enable_by_count()
                    return func(*args, **kwargs)
                finally:
                    profiler.print_stats()
            return profiled_func
        return inner
    
def get_number():
    for x in xrange(5000000):
        yield x


#@njit(fastmath=True)#, parallel=True)
#@profile
#@do_profile(follow=[get_number])
def prox_g_i_conj_new(y, rho, v_y, gamma):
    """
    Proximal operator of the conjugate of indicator function of all
    balls on the specific level. Implementation is based on the
    Moreaux identity.

    y: 2-dim array os shape ...
    rho: a proximal paramter > 0
    v_y: array of the same shape as y. The center of the balls
    gamma: a positive number. A radius of the balls
    """

    n, m = y.shape
    L = int(np.sqrt(m))
    # difference between the center v_y of the ball and a point  y/rho (blockwise)
    diff = y/rho - v_y
    norms = LA.norm(diff, axis=1)
    mask = norms > gamma
    #indices = mask.nonzero()[0]
    #ind_len  = indices.shape[0]
    z = np.zeros(y.shape)
    #if ind_len > 0:
    if True in mask:
        proj = ((diff[mask].T * gamma/norms[mask]).T + v_y[mask])
        z[mask] = y[mask] - rho * proj
    return z

def psnr(x, y, vmax=-1):
    if vmax < 0:
        m1 = abs(x).max()
        m2 = abs(y).max()
        vmax = max(m1, m2)
    d = np.mean((x - y) ** 2)
    return 10 * np.log10(vmax ** 2 / d)

def snr(x, x_true):
    d = LA.norm(x - x_true)
    return 20 * np.log10(LA.norm(x_true)/d)

def TV(x):
    G = grad(x)
    return np.sum(LA.norm(G,axis=2))

def Huber(x, alpha):
    G = grad(x)
    norm_G = LA.norm(G,axis=2)
    s = np.sum( np.where( np.abs(norm_G) < alpha, 0.5*norm_G**2/alpha, norm_G - 0.5*alpha ))
    return s

def Laplacian(x):
    G = grad(x)
    return 0.5 * np.vdot(G, G)

def define_energy(reg):
    """Choose energy function depending on the paramter reg"""
    
    if reg == -1:
        energy = Laplacian
    elif reg == 0:
        energy = TV
    else:
        energy = lambda x: Huber(x, reg)
        
    return energy


def grad(x):
    """
    Computes the gradient of an image x
    """
    h, w = x.shape
    G = np.zeros((h, w, 2), x.dtype)
    G[:, :-1, 0] = x[:, 1:] - x[:, :-1]
    G[:-1, :, 1] = x[1:] - x[:-1]
    return G

def grad_T(G):
    """
    -div of G = transpose of grad
    """
    h, w = G.shape[:2]
    I = np.zeros((h, w), G.dtype)
    I[:, :-1] -= G[:, :-1, 0]
    I[:, 1: ] += G[:, :-1, 0]
    I[:-1]    -= G[:-1, :, 1]
    I[1: ]    += G[:-1, :, 1]
    return I

def prox_norm_2(x, eps):
    """
    Proximal operator of function f = 0.5*eps*||x||**2,
    """
    return x  / (eps + 1)


def prox_norm_21_conj(y, eps):
    """
    Proximal operator of the ||y||_{2,1} norm

    y: (N x N x 2) array
    eps: a positive number, stepsize
    """

    norm = np.sqrt((y**2).sum(axis=-1))
    return y/(np.fmax(1,norm)[:,:,np.newaxis])

def prox_Huber_conj(y, a,  eps):
    """
    Proximal operator of the conjugate of the Huber
    function 
    |x|_a = 1/(2*a) |x|^2  if |x|<=a,
            |x| - a/2,     otherwise

    y: (N x N x 2) array
    a: a positive number
    eps: a positive number, stepsize
    """
    z = y/(1+a*eps) 
    norm = np.sqrt((z**2).sum(axis=-1))
    return z/(np.fmax(1, norm)[:,:,np.newaxis])

def choose_prox_y(reg):
    if reg == -1:
        prox = prox_norm_2
    elif reg == 0:
        prox = prox_norm_21_conj
    else:
        prox = lambda x, eps: prox_Huber_conj(x, reg, eps)
        
    return prox

def convolution(x, psf):
    """Computes convolution of the image x with the point spread function psf."""
    return fftconvolve(x, psf, mode='same')

def convolution_T(x, psf):
    """Computes the trasnpose of the convolution of the image x with the point spread function psf."""
    return np.rot90(fftconvolve(np.rot90(x,2), psf, 'same'), 2)


def normalize_intensity(img):
    """ Make image with arbitrary real values normalized, i.e. put them
    into [0.1] interval.

    """
    lmin = img.min()
    lmax = img.max()
    return (img - lmin) / (lmax - lmin)


def proj_onto_strip(x, v, alpha):
    """Compute projection of x onto the set {u: |<1, u-v>| <= alpha}"""
    
    n = x.shape[0]
    vs = v.sum()
    xs = x.sum()
    a1 = vs - alpha
    a2 = vs + alpha 
    if xs < a1:
        x += (a1 - xs)/n
    elif xs > a2:
        x += (a2 - xs)/n
    else:
        pass
    
    return x

def error_dict(x_true, x_rec, L):
    """
    Make a dictionary d for every level s.t.
    d[i] is array of all errors for two images on this scale
    """
    u = x_true - x_rec
    d = dict()
    for i in range(1, L+1):
        d[i] = np.abs(np.sum(extract_patches(u, i), axis=(2,3)).ravel())
        print("Level", i, ", mean, max=", np.mean(d[i]), np.max(d[i]))
    return d


def error_dict2(x_true, x_rec, L, q=1.):
    """
    Make a dictionary d for every level s.t.
    d[i] is max of all errors for two images on this scale
    q is a number from [0,1]
    """
    u = x_true - x_rec
    d = dict()
    for i in range(1, L+1):
        d[i] = q*max(np.abs(np.sum(extract_patches(u, i), axis=(2,3)).ravel()))
        #print("Level", i, ", mean, max=", np.mean(d[i]), np.max(d[i]))
    return d


def error_dict3(x_true, x_rec, L, q=1.):
    """
    Make a dictionary d for every level s.t.
    d[i] is max of all errors for two images on this scale
    q is a number from [0,1]
    """
    u = x_true - x_rec
    d = dict()
    for i in range(1, L+1):
        kernel = np.ones((i,i))
        d[i] = q*max(np.abs(transform_x_into_y(u, kernel).ravel()))
        #print("Level", i, ", mean, max=", np.mean(d[i]), np.max(d[i]))
    return d

def error_dict_quantile(x_true, x_rec, L, q=0.8):
    """
    Make a dictionary d for every level s.t.
    d[i] is max of all errors for two images on this scale
    q is a number from [0,1]
    """
    u = x_true - x_rec
    d = dict()
    for i in range(1, L+1):
        kernel = np.ones((i,i))
        arr =  (np.abs(transform_x_into_y(u, kernel).ravel()))
        d[i] = np.quantile(arr, q)
        print("Level", i, ", mean, max=", np.mean(arr), np.max(arr))
    return d


def error_dict_quantile_diff(x_true, x_rec, L, q=0.8, r=1.1):
    """
    Make a dictionary d for every level s.t.
    d[i] is max of all errors for two images on this scale
    q is a number from [0,1] for the first level
    """
    u = x_true - x_rec
    d = dict()
    for i in range(1, L+1):
        kernel = np.ones((i,i))
        arr =  (np.abs(transform_x_into_y(u, kernel).ravel()))
        q_lvl = q - r**i/10
        d[i] = np.quantile(arr, q_lvl)
        print("Level", i, ", mean, max=", np.mean(arr), np.max(arr))
    return d


def error_dict_full(x_true, x_rec, L):
    """
    Make a dictionary d for every level s.t.
    d[i] collects all errors for two images on this scale
    """
    u = x_true - x_rec
    d = dict()
    for i in range(1, L+1):
        kernel = np.ones((i,i))
        arr =  np.abs(transform_x_into_y(u, kernel))
        d[i] = arr
        print("Level", i, ", mean, max=", np.mean(arr), np.max(arr))
    return d



def transform_y_into_x(y, kernel):
    i = kernel.shape[0]
    if i == 1:
        x = y
    else:
        x = fftconvolve(y, kernel)
    return x


def transform_x_into_y(x, kernel):
    i = kernel.shape[0]
    if i == 1:
        y = x
    else:
        y = ndimage.convolve(x, kernel, origin=-1)[(i-1):, (i-1):] 
    return y

def transform_x_into_y_diff_implementation(x, kernel):
    i = kernel.shape[0]
    if i == 1:
        y = x
    else:
        x1 = extract_patches(x, i)
        y = np.sum(x1, axis=(2,3))
        #y = ndimage.convolve(x, kernel, origin=-1)[(i-1):, (i-1):] 
    return y

def define_Y(x, N, lvl):
    Y = np.empty(shape=lvl+1, dtype=object)
    Y[0] = np.zeros((N, N, 2))
    for i in range(1, lvl+1):
        Y[i] = np.zeros((N - i + 1, N - i + 1))
        #Y[i] = spr.csr_matrix((n, i**2))
        #Y[i] = spr.lil_matrix((n, i**2))
    return Y


def extract_patch(img, x, y, d, path):
    """
    Extract a patch from the image and save it where I need.
    x, y: are top left corner
    d: is the length of a side
    """
    z = img[x:x+d, y:y+d]
    plt.imsave(z, '~/temp/SFB755_volume/Buch/Originals/Teil2/Nanoscopy/LukeMalitsky/figures')


def make_image(data, outputname, size=(3, 3), dpi=1000):
    fig = plt.figure()
    fig.set_size_inches(size)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    plt.set_cmap('gray')
    ax.imshow(data, aspect='equal')
    plt.savefig(outputname, dpi=dpi)
