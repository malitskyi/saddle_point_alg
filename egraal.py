#!/usr/bin/env python
"""Implmentation of EGRAAL for the statistical multiresolution
estimation for image reconstruction.

Here is the main variable is u = (x, y, z_1, ..., z_lvl), where x in N
x N array, y is the dual variable for the total variation term and is
N x N x 2 array and z_i is the dual variable that corresponds to the
i-th lever of resolution and it is a 1d array of length (N-i+1)**2

"""

import numpy as np
from numpy.polynomial import Polynomial as poly
import scipy.linalg as LA
import scipy.io as sio
import scipy.sparse as spr
import matplotlib.pyplot as plt
#from itertools import product
#import os
#from sklearn.feature_extraction.image import extract_patches, extract_patches_2d

from skimage import data
from skimage import io
from skimage import img_as_float
from skimage.color import rgb2gray
from skimage import transform

from time import perf_counter
from misc_functions import *
import warnings


__author__ = "Yura Malitsky"
__license__ = "MIT License"
__email__ = "y.malitskyi@math.uni-goettingen.de"
__status__ = "Development"


def F(u, v, gamma, psf):
    """Defines a monotone operator F which comes from KKT condition.

    Parameters
    ----------
    u: 1d object array = [x, y, z_1,..., z_m]
    v: 2d array.
       Given image.
    psf: 2d array.
       Kernel for a convolution.
   
    Returns
    -------
    An object array as u.
    """
    p = len(u)
    lvl = p - 2
    x, y = u[0], u[1]
    u_new = np.empty(shape=p, dtype=object)
    u_new[1] = -grad(x)
    
    if psf is not None:
        x = convolution(x, psf)

    res = x - v # Ax - v
    x_new = 0
    for i in range(1, p-1):
        # i-th level
        kernel = np.ones((i,i))
        x_into_y = transform_x_into_y(res, kernel)
        #x_into_y = transform_x_into_y_format(res, i)
        zi = u[i+1]
        #print(i, zi.shape)
        x_new += transform_y_into_x(zi * x_into_y, kernel)
        u_new[i+1] = (gamma[i]**2 - x_into_y**2) # -h_s(x)
        
    if psf is not None:
        x_new = 2 * convolution_T(x_new, psf)
    else:
        x_new *= 2

    u_new[0] = x_new + grad_T(y)
    return u_new
    
def Prox_G(u, rho, reg):
    """Computes the proximal operator for VI

    Parameters
    ----------

    u: 1d object array = [x, y, z_1,..., z_m].
    rho: 1d array of parameters.
    reg: -1 or other value from [0,1]. 
       -1 refers to Laplacian, 0 to TV, others -- to Huber function

    Returns
    -------
    An object array as u.

    """
    p = len(u)
    prox_y = choose_prox_y(reg)
    u[1] = prox_y(u[1], rho[1])
    for i in range(2, p):
        u[i] = np.fmax(u[i], 0)
        
    return u

def define_initial_u(v, lvl,  eps=1e-3):
    """ Defines initial vector u^0 for GRAAL to start from.

    Paremeters
    ----------
    v: 2d array, given image.
    lvl: number of levels of resolution.
    
    Returns
    -------
    An object array u.

    """
    p = lvl+2
    N = v.shape[0]
    u = np.empty(shape=p, dtype=object)
    u[0] = v + eps*np.random.randn(N, N)
    #u[0] = eps*np.random.rand(N, N)
    u[1] = grad(u[0])
    for i in range(2, p):
        k = (N - (i-1) + 1)
        u[i] = np.zeros((k, k))
    return u

def egraal(gamma, lvl, v, weights, reg=0, numb_iter=100, psf=None):
    """Implementation of the adaptive  GRAAL algorithm [1]
    for solving SMRE problem
    
    Parameters
    ----------
    gamma is an 1 x lvl array of positive numbers. Measures disrepancy
    on different resolutions between Ax and x0
    lvl: positive integer number of levels in multiscaling approach
    weights: 1 x (lvl+2) array of positive real weights. 
       Imposes some metric.
    reg: scalar -1 or from [0,1]. 
       -1 refers to Laplacian, 0 to TV, others -- to Huber function
    numb_iter: positive integer
       Number of iterations.
    psf: 2d array.
       Kernel for a convolution.

    Returns
    -------
    error: list of nonnegative numbers.
      Collects all residuals |u^k-u^{k-1}|

    u: object array.
      Returns last iterate of the algorithm

    References
    ----------
    [1]: Malitsky Y. "Golden ratio algorithms for variational
    inequalities." arXiv:1803.08832 (2018)

    """

    begin = perf_counter()
    u_old = define_initial_u(v, lvl,  eps=1e-5)
    u = define_initial_u(v, lvl)
    u_bar = u
    th = 1
    Fu_old = F(u_old, v, gamma, psf)
    error = []
    
    for i in range(numb_iter):
        norm_u = norm_square_object(u - u_old, 1/weights)
        Fu = F(u, v, gamma, psf)
        norm_F = norm_square_object(Fu - Fu_old, weights)
        if i == 0:
            print("norm_ratio", norm_u/norm_F)
            alpha =  min(3 * th/8 * norm_u/norm_F, 5)
            th = 1.5
        else:
            alpha = min(10/9 * alpha_old,  3*th/8 * norm_u/norm_F, 5)
            ################################# Check this.
            th = 1.5 * alpha/alpha_old
            
        #print(alpha)
        u_old = u
        u_bar = (u + 2 * u_bar) / 3
        #err = 2*np.sqrt(norm_square_object(u_bar - u))
        u = Prox_G(u_bar - alpha * weights * Fu, alpha * weights, reg)
        
        #err += np.sqrt(norm_square_object(u_bar - u))
        #err = norm_square_object(u_old - Prox_G(u_old - weights * Fu, weights, reg))
        err = np.sqrt(norm_square_object(u_old - u))
        #print(i, err)
        error.append(err)
        Fu_old = Fu
        alpha_old = alpha
        
    end = perf_counter()
    print("EGRAAL, time execution", end - begin)
    return error, u

def check_monotonicity_F(n_max, v, gamma, lvl, psf=None):
    """ Auxiliary function that checks if the operator constructed by
    F defined above is monotone.

    Of course the function can only check this for finite number of
    points.
    
    Parameters 
    ---------- 
    n_max: positive integer.
         How many times to check
    
    v: initial image.
         Only to determine right dimensions.
    gamma: list (dictionary) of nonnegative numbers.
    lvl: positive integer.
         number of levels.
    psf: 2d array or None
         Kernel for a convolution.

    """
    ind = 0
    N = v.shape[0]
    for _ in range(n_max):
        u1 = np.empty(shape=lvl+2, dtype=object)
        u2 = np.empty(shape=lvl+2, dtype=object)
        u1[0] = np.random.randn(N,N)
        u2[0] = np.random.rand(N,N)
        u1[1] = grad(u1[0])
        u2[1] = grad(u2[0])
        for i in range(2, lvl+2):
            r = N - i + 2
            u1[i] = np.abs(np.random.randn(r, r))
            u2[i] = np.random.rand(r, r)*10
        q1 = F(u1, v, gamma, psf) -  F(u2, v, gamma, psf)
        q2 = u1 - u2
        res = dot_object(q1, q2)
        if res >= -1e-8:
            ind += 1
        print(res)
        
    print("# of success: {0}/ # all trials: {1}".format(ind, n_max))

