#!/usr/bin/env python

"""
SMRE for siemens image. Experiments with different algorithms.

Set parameter algorithms to a list of numbers, where
1 is explicit PDHG
2 is block-coordinate PDHG
3 is EGRAAL
4 is explicit PDHG but with different regularizers: TV, Huber with 0.5, and Laplacian
5 is block-coordinate PDHG but with different regularizers: TV, Huber with 0.5, and Laplacian
6 is EGRAAL but with different regularizers: TV, Huber with 0.5, and Laplacican


"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as LA
import scipy.io as sio
import warnings
from skimage import data
from skimage import io
import matplotlib.pyplot as plt
from skimage import img_as_float
from skimage.color import rgb2gray
from skimage import transform

from block_coordinate import block_coordinate_pdhg
from explicit_pdhg import  explicit_pdhg
from egraal import egraal
from misc_functions import *
from better_plots import *
from save_data import *



__author__ = "Yura Malitsky"
__license__ = "MIT License"
__email__ = "y.malitskyi@math.uni-goettingen.de"
__status__ = "Development"


##################################

warnings.filterwarnings("ignore")
set_matplotlib()
# define which algorithms to run
algorithms = [4]
#algorithms = [1, 2, 3]

# fix number of iterations
n_it = 300


# choose regularizer
#reg = 0.5 # Huber function with 0.5
reg = 0 # total variation
#reg = -1 # laplacian
J = define_energy(reg)

# choose number of level resolutions
lvl = 3


# define noise parameters
noise_lvl = 0.2
#noise_lvl = 0.1

gamma = {i: noise_lvl * i**2 for i in range(1, lvl+1)}
# gamma = {}
# for i in range(1, lvl+1):
#     gamma[i] = noise_lvl * i**2

# read the image
N = 256
img_siemens_complex = sio.loadmat("input-images/Siem_noisy_QNAvP_u.mat")['u']
#img = np.angle(img_siemens_complex)
img = np.real(img_siemens_complex)

#img_true = img_as_float(rgb2gray(io.imread("input-images/Goettingen.jpg")))[:N,:N]
#img = img_true + np.random.randn(N,N)*noise_lvl

v = img.copy()
psf = None # this is just a denoising problem



# define important ingredients for each of the algorithm

## explicit PDHG
if 1 in algorithms:
    tau  = 0.01
    Sigma = np.ones(lvl+1)


    ans1 = explicit_pdhg(gamma, lvl, v, tau, Sigma, reg=reg,
                         numb_iter=n_it, psf=None)
    x1 = ans1[2]
    err_feas1 = ans1[0]
    err_gap1 = ans1[1]
    succ_error1 = ans1[-1]
    print("Energy: original {0:.0f}, final {1:.0f}".format(J(v), J(x1)))
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15,4))
    ax[0].imshow(v, cmap='gray')
    ax[1].imshow(x1, cmap='gray')
    #ax[2].plot(err_feas, 'b')
    #ax[2].plot(err_gap, 'g')
    ax[2].plot(succ_error1, 'b')
    ax[2].set_yscale('log')

## block-coordinate PDHG
if 2 in algorithms:
    tau = 1
    ans2 = block_coordinate_pdhg(gamma, lvl, v, tau, reg=reg,
                             numb_iter=n_it, psf=None)

    x2 = ans2[2]
    err_feas2 = ans2[0]
    err_gap2 = ans2[1]
    succ_error2 = ans2[-1]

    print("Energy: original {0:.0f}, final {1:.0f}".format(J(v), J(x2)))
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15,4))
    ax[0].imshow(v, cmap='gray')
    ax[1].imshow(x2, cmap='gray')
    ax[2].plot(succ_error2, 'b')
    ax[2].set_yscale('log')
    
## egraal
if 3 in algorithms:
    weights = np.ones(lvl+2)
    weights[0] = 0.001
    #weights[0] = 0.001 # for huber
    #weights[1] = 1

    ans3 = egraal(gamma, lvl, v, weights, reg=reg,
              numb_iter=n_it, psf=psf)
    x3 = ans3[1][0]
    succ_error3 = ans3[0]

    print("Energy: original {0:.0f}, final {1:.0f}".format(J(v), J(x3)))
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15,4))
    ax[0].imshow(v, cmap='gray')
    ax[1].imshow(x3, cmap='gray')
    ax[2].plot(succ_error3, 'b')
    ax[2].set_yscale('log')

    
## explicit_PDHG for different regularizers
if 4 in algorithms:
    #filename = 'figures/results/gaenseliesel'
    filename = 'figures/results/siemens/epdhg-siemens'
    #tau  = 0.01
    #tau = 0.1
    tau = 0.1
    Sigma = np.ones(lvl+1)
    Sigma[0] = 0.01
    ans1 = explicit_pdhg(gamma, lvl, v, tau, Sigma, reg=0,
                         numb_iter=n_it, psf=None)
    
    ans2 = explicit_pdhg(gamma, lvl, v, tau, Sigma, reg=0.5,
                         numb_iter=n_it, psf=None)
    
    ans3 = explicit_pdhg(gamma, lvl, v, tau, Sigma, reg=-1,
                         numb_iter=n_it, psf=None)

    
    np.save('{0}-full_ans.npy'.format(filename), [ans1, ans2, ans3])
    x1, x2, x3 = ans1[2], ans2[2], ans3[2]
    succ_error1 = np.array(ans1[-1])
    succ_error2 = np.array(ans2[-1])
    succ_error3 = np.array(ans3[-1])
    save_data(filename, x1, x2, x3, succ_error1, succ_error2,
              succ_error3, v)


## coordinate PDHG for different regularizers
if 5 in algorithms:
    #filename = 'figures/results/gaenseliesel'
    filename = 'figures/results/siemens/coo_pdhg-siemens'
    tau = 1
    #tau = 0.01
    ans1 = block_coordinate_pdhg(gamma, lvl, v, tau, reg=0,
                             numb_iter=n_it, psf=None)
    ans2 = block_coordinate_pdhg(gamma, lvl, v, tau, reg=0.5,
                             numb_iter=n_it, psf=None)
    ans3 = block_coordinate_pdhg(gamma, lvl, v, tau, reg=-1,
                             numb_iter=n_it, psf=None)

    np.save('{0}-full_ans.npy'.format(filename), [ans1, ans2, ans3])
    x1, x2, x3 = ans1[2], ans2[2], ans3[2]
    succ_error1 = np.array(ans1[-1])
    succ_error2 = np.array(ans2[-1])
    succ_error3 = np.array(ans3[-1])
    save_data(filename, x1, x2, x3, succ_error1, succ_error2,
              succ_error3, v, epochs=True)


## GRAAL for different regularizers
if 6 in algorithms:
    #filename = 'figures/results/gaenseliesel'
    filename = 'figures/results/siemens/graal-siemens'

    # Save files? Be careful, it can overwright previous ones.
    SAVE = True
    
    weights = np.ones(lvl+2)
    weights[0] = 1e-3    
    weights[1] = 1e-2

    #weights[0] = 0.001 # for huber

    print(weights, noise_lvl)
    ans1 = egraal(gamma, lvl, v, weights, reg=0,
                  numb_iter=n_it, psf=None)

    ans2 = egraal(gamma, lvl, v, weights, reg=0.5,
                  numb_iter=n_it, psf=None)
    
    ans3 = egraal(gamma, lvl, v, weights, reg=-1,
                  numb_iter=n_it, psf=None)
    
    x1, x2, x3 = ans1[1][0], ans2[1][0], ans3[1][0]
    succ_error1 = np.array(ans1[0])
    succ_error2 = np.array(ans2[0])
    succ_error3 = np.array(ans3[0])
    
    if SAVE:
        np.save('{0}-full_ans.npy'.format(filename), [ans1, ans2, ans3]) # 
        save_data(filename, x1, x2, x3, succ_error1, succ_error2,
              succ_error3, v)
