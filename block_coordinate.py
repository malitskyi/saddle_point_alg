#!/usr/bin/env python
"""
Block-coordinate PDHG for statistical multiresolution estimation
for image reconstruction.

"""

import numpy as np
import scipy.linalg as LA
import scipy.io as sio
import matplotlib.pyplot as plt
from sklearn.feature_extraction.image import extract_patches
from skimage import data
from skimage import io
from skimage import img_as_float
from skimage.color import rgb2gray
from skimage import transform
from time import perf_counter
from misc_functions import *
#from simulate_noise import define_gamma
import warnings

__author__ = "Yura Malitsky"
__copyright__ = "Copytight 2018, Yura Malitsky"
__license__ = "MIT License"
__email__ = "y.malitskyi@math.uni-goettingen.de"
__status__ = "Development"




def prox_G_conj(i, yi, rho, v_dict, gamma, reg):
    """Proximal operator of g^*.

    Parameters
    ----------
    Y: object of lvl+1 arrays. 
        Dual variable.
    rho: positive number.
    
    v_dict: dictionary with keys 1 ,..., lvl v_dict[i] has the same
        shape as Y[i] 
    gamma: 1 d array reg: scalar value.  
        If reg = 0, we use TV, if reg = -1 -- Laplacican, if reg
        between 0 and 1 -- Huber function with reg.
    reg:  -1 or other value from [0,1]. 
       -1 refers to Laplacian, 0 to TV, others -- to Huber function
    Returns
    -------
    An array of yi shape

    """    
    if i == 0:
        prox_y = choose_prox_y(reg)
        yi_new = prox_y(yi, rho)
    else:
        yi_new = prox_g_i_conj(yi, rho, v_dict[i], gamma[i])
    
    return yi_new


def prox_g_i_conj(y, rho, v_y, gamma):
    """Proximal operator of the conjugate of the indicator function of all
    stripes on the specific level: | x_1 + ... + x_p - v_1 - ... - v_p | <= gamma

    It is computed for a specific level of resolution.

    Parameters
    ----------
    y: 1-dim array
    rho: a proximal paramter > 0
    v_y: array of the same shape as y. 
    gamma: a positive number. 

    Returns
    -------
    An array of y shape.
    
    """
    n = y.shape[0]
    z = np.zeros(y.shape)

    diff = y - rho * v_y
    a = rho * gamma
    mask1 = diff < -a
    mask2 = diff > a
    z[mask1] = (diff + a)[mask1]
    z[mask2] = (diff - a)[mask2]
    return z

def K_i(i_lvl, x, psf=None):
    """Computes i_lvl block in the Kx for the coordinate primal-dual method.
    
    When psf (point spread function) is None, it is just reformatting
    of x. When psf is not None, then for all blocks except the first
    one (that corresponds to the gradient) we first compute
    convolution of x, and then do the same reformatting.
    
    Parameters
    ----------
    i_lvl: nonnegative integer
       Defines the level number.
    x: 2d array.
    psf: 2d array, None by default. 
       Kernel for convolution.
    Returns
    -------
    Object array of length lvl+1.

    """
    if i_lvl == 0:
        y = grad(x)
    else:
        if psf is not None:
            x = convolution(x, psf)
        kernel = np.ones((i_lvl, i_lvl))
        y = transform_x_into_y(x, kernel)
    return y

def KT_i(i_lvl, y, N, psf=None):
    """Computes i_lvl block in the KTy for the coordinate primal-dual method.
   
    Parameters
    ----------
    i_lvl: nonnegative integer
       Defines number of levels.
    Y: object array of length lvl + 1.
    N: nonnegative integer.
       img.shape[0]
    psf: 2d array, None by default. 
       It is a kernel for convolution.
    Returns
    -------
    2d array.

    """
    x = 0
    if i_lvl == 0:
        x = grad_T(y)
    else:
        kernel = np.ones((i_lvl, i_lvl))
        x += transform_y_into_x(y, kernel)

    if psf is not None and i_lvl != 0:
        x = convolution_T(x, psf)
    return x

def block_coordinate_pdhg(gamma, lvl, v, tau, reg=0,
                          numb_iter=100, psf=None):
    """Implementation of the block-coordinate primal-dual algorithm [1]
    for solving SMRE in the form of min_x g_0(A_0x)+ ... + g_m(A_mx).

    Here g_0 is some regularizer, A_0 is the gradient of the image; g_s
    = Sum_{i\in s} (y_i - x0_i)^2 A_s = P_s A, where P_s is a
    projection onto the square s and A is the forward operator
    (identity, convolution, etc.)

    
    Parameters
    ----------
    gamma is an 1 x lvl array of positive numbers. Measures disrepancy
    on different resolutions between Ax and x0
    lvl: positive integer number of levels in multiscaling approach
    tau: positive number
    sigma: 1 x (lvl+1) array of positive real weights.
    reg: scalar -1 or from [0,1]. 
       -1 refers to Laplacian, 0 to TV, others -- to Huber function
    numb_iter: positive integer
       Number of iterations.
    psf: 2d array.
       Kernel for a convolution.

    Returns
    -------
    optim_gap: list of nonnegative numbers.
             Optimality gaps, computed in every iteration.
    feas_gap: list of nonnegative numbers.
             Feasibility gaps, computed in every iteration.
    x: 2d array.
      Reconstructed image.
    Y: 1d object array.
      Dual solutions.
    succ_error: list of nonnegative numbers.
      Collects all residuals |u^k-u^{k-1}|
    References
    ----------
    [1] D.R. Luke, Y. Malitsky. "Block-coordinate primal-dual method for
    nonsmooth minimization over linear constraints", Distributed and
    Large-Scale Optimization P. Giselsson and A. Rantzer ed. Springer,
    2018. DOI https://doi.org/10.1007/978-3-319-97478-1_6 
    (or arxiv: https://arxiv.org/abs/1801.04782 )

    """
    begin = perf_counter()
    N = v.shape[0]
    x = v.copy()
    square_norms_K = 1.0 * np.arange(lvl+1)**4
    square_norms_K[0] = 8.0 # gradient norm
    
    v_dict = make_v_dict(v, lvl) # format v for different levels
    sigma = 1 / (square_norms_K * tau)
    Y = define_Y(x, N, lvl)
    u = 0
    feas_gap = []
    optim_gap = []
    succ_error = []
    m = lvl + 1
    np.random.seed(0)
    permut = np.arange(m)
    
    for epoch in range(numb_iter):
        # in each epoch we permute blocks randomly without substitutions
        np.random.shuffle(permut)
        Y_old = Y.copy() # this is stupid but I have to keep it for checking the error
        x_old = x.copy()
        for ik in permut:
            y_ik = Y[ik]
            Kx_ik = K_i(ik, x, psf)
                        
            y_ik_new = prox_G_conj(ik, y_ik + sigma[ik] * Kx_ik, sigma[ik], v_dict, gamma, reg)
            res = -KT_i(ik, y_ik_new - y_ik, N, psf)
            u += (tau/m * res)
            x_incr = (u +  tau * res)
            x += x_incr
            Y[ik] = y_ik_new
            
        feas_gap.append(LA.norm(u))
        optim_gap.append(np.sqrt(norm_square_object(Y_old - Y, 1/sigma)))
        residual = np.sqrt(LA.norm(x-x_old)**2 + norm_square_object(Y_old - Y))
        succ_error.append(residual)
        
    end = perf_counter()
    print("Block-coordinate PDHG, time execution:", end-begin)
    return optim_gap, feas_gap, x, Y, succ_error

