#!/usr/bin/env python
"""
SMRE for tubuline image. Experiments with different algorithms.

Set parameter algorithms to a list of numbers, where
1 is explicit PDHG
2 is block-coordinate PDHG
3 is EGRAAL
4 is explicit PDHG but with different regularizers: TV, Huber with 0.5, and Laplacian
5 is block-coordinate PDHG but with different regularizers: TV, Huber with 0.5, and Laplacian
6 is EGRAAL but with different regularizers: TV, Huber with 0.5, and Laplacican
"""

import numpy as np
import scipy.linalg as LA
import scipy.io as sio
import warnings
import matplotlib.pyplot as plt
from skimage import io
from skimage import img_as_float
from skimage.color import rgb2gray
from skimage import transform

from block_coordinate import block_coordinate_pdhg
from explicit_pdhg import  explicit_pdhg
from egraal import egraal

from misc_functions import *
from save_data import *


__author__ = "Yura Malitsky"
__license__ = "MIT License"
__email__ = "y.malitskyi@math.uni-goettingen.de"
__status__ = "Development"



##################################

warnings.filterwarnings("ignore")
# define which algorithms to run

#algorithms = [1]
algorithms = [4, 5, 6]
#algorithms = [4,5,6]

# fix number of iterations
n_it = 1000


# choose regularizer
#reg = 0.5 # Huber function with 0.5
reg = 0 # total variation
#reg = -1 # laplacian

J = define_energy(reg)


# read the image
N = 976
img_big = normalize_intensity(io.imread(
    "input-images/Tubulin_Jennifer_STED_2Det_pix10nm.tiff", as_grey=True))
img = img_big[:N, :N]
psf = io.imread("input-images/Tubulin_Jennifer_STED_PSF_pix10nm.tiff")
psf = psf / psf.sum()
print("Image size is", img.shape)
#psf = None

v = img.copy()
# choose number of level resolutions
lvl = 10
print("Number of resolution levels is", lvl)

# define noise parameters
noise_lvl = 0.05

gamma = {i: noise_lvl * i**2 for i in range(1, lvl+1)}
# gamma = {}
# for i in range(1, lvl+1):
#     gamma[i] = noise_lvl * i**2



# define important ingredients for each of the algorithm

## explicit PDHG
if 1 in algorithms:
    tau  = 1
    Sigma = np.ones(lvl+1)
    Sigma[0] = 1e3

    ans1 = explicit_pdhg(gamma, lvl, v, tau, Sigma, reg=reg,
                         numb_iter=n_it, psf=psf)
    x1 = ans1[2]
    x1 = np.clip(x1, 0, 1.4)
    err_feas = ans1[0]
    err_gap = ans1[1]
    succ_error = ans1[-1]
    print("Energy: original {0:.0f}, final {1:.0f}".format(J(v), J(x1)))
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15,4))
    ax[0].imshow(v, cmap='gray')
    ax[1].imshow(x1, cmap='gray')
    #ax[2].plot(err_feas, 'b')
    #ax[2].plot(err_gap, 'g')
    ax[2].plot(succ_error, 'b')
    ax[2].set_yscale('log')

## block-coordinate PDHG
if 2 in algorithms:
    filename = 'figures/results/siemens/coo_pdhg-tubulin'

    tau = 1
    ans2 = block_coordinate_pdhg(gamma, lvl, v, tau, reg=reg,
                                 numb_iter=n_it, psf=psf)

    x2 = ans2[2]
    x2 = np.clip(x2, 0, 1.4)
    err_feas2 = ans2[0]
    err_gap2 = ans2[1]
    succ_error2 = ans2[-1]

    print("Energy: original {0:.0f}, final {1:.0f}".format(J(v), J(x2)))
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15,4))
    ax[0].imshow(v, cmap='gray')
    ax[1].imshow(x2, cmap='gray')
    #ax[2].plot(err_feas, 'b')
    #ax[2].plot(err_gap, 'g')
    ax[2].plot(succ_error2, 'b')
    ax[2].set_yscale('log')
    
    plt.imsave('{0}-block_pdhg_TV.png'.format(filename), x2, cmap='gray')
    
## egraal
if 3 in algorithms:
    weights = np.ones(lvl+2)
    weights[0] = 10
    #weights[1] = 1

    ans3 = egraal(gamma, lvl, v, weights, reg=reg,
              numb_iter=n_it, psf=psf)
    x3 = ans3[1][0]
    x3 = np.clip(x3, 0, 1.4)
    succ_error3 = ans3[0]



    print("Energy: original {0:.0f}, final {1:.0f}".format(J(v), J(x3)))
    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(15,4))
    ax[0].imshow(v, cmap='gray')
    ax[1].imshow(x3, cmap='gray')
    ax[2].plot(succ_error3, 'b')
    ax[2].set_yscale('log')


## explicit PDHG for different regularizers
if 4 in algorithms:
    filename = 'figures/results/tubulin/epdhg-tubulin_sigma'
    #n_it = 1000
    tau  = 0.1
    Sigma = np.ones(lvl+1)
    Sigma[0] = 1e1

    ans1 = explicit_pdhg(gamma, lvl, v, tau, Sigma, reg=0,
                         numb_iter=n_it, psf=psf)
    ans2 = explicit_pdhg(gamma, lvl, v, tau, Sigma, reg=0.5,
                         numb_iter=n_it, psf=psf)
    ans3 = explicit_pdhg(gamma, lvl, v, tau, Sigma, reg=-1,
                         numb_iter=n_it, psf=psf)
    
    print('tau', tau, 'noise_lvl', noise_lvl)
    np.save('{0}-full_ans.npy'.format(filename), [ans1, ans2, ans3])
    x1, x2, x3 = ans1[2], ans2[2], ans3[2]
    succ_error1 = np.array(ans1[-1])
    succ_error2 = np.array(ans2[-1])
    succ_error3 = np.array(ans3[-1])
    save_data(filename, x1, x2, x3, succ_error1, succ_error2,
              succ_error3, v)
    
    x1_ = np.clip(x1, 0, 1.4)
    x2_ = np.clip(x2, 0, 1.4)
    x3_ = np.clip(x3, 0, 1.4)
    save_data(filename+'_clip', x1_, x2_, x3_, succ_error1, succ_error2,
              succ_error3, v)

    
## coordinate PDHG for different regularizers
if 5 in algorithms:
    #filename = 'figures/results/gaenseliesel'
    filename = 'figures/results/tubulin/coo_pdhg-tubulin'
    tau = 1
    #tau = 0.01
    ans1 = block_coordinate_pdhg(gamma, lvl, v, tau, reg=0,
                                 numb_iter=n_it, psf=psf)
    ans2 = block_coordinate_pdhg(gamma, lvl, v, tau, reg=0.5,
                                 numb_iter=n_it, psf=psf)
    ans3 = block_coordinate_pdhg(gamma, lvl, v, tau, reg=-1,
                                 numb_iter=n_it, psf=psf)

    np.save('{0}-full_ans.npy'.format(filename), [ans1, ans2, ans3])
    x1, x2, x3 = ans1[2], ans2[2], ans3[2]
    succ_error1 = np.array(ans1[-1])
    succ_error2 = np.array(ans2[-1])
    succ_error3 = np.array(ans3[-1])
    save_data(filename, x1, x2, x3, succ_error1, succ_error2,
              succ_error3, v, epochs=True)
    
    x1_ = np.clip(x1, 0, 1.4)
    x2_ = np.clip(x2, 0, 1.4)
    x3_ = np.clip(x3, 0, 1.4)
    save_data(filename+'_clip', x1_, x2_, x3_, succ_error1, succ_error2,
              succ_error3, v, epochs=True)

    
## GRAAL for different regularizers    
if 6 in algorithms:
    filename = 'figures/results/tubulin/egraal-tubulin'
    #n_it = 20

    weights = np.ones(lvl+2)
    weights[0] = 1e-2
    weights[1] = 1
    print(weights, noise_lvl)
    ans1 = egraal(gamma, lvl, v, weights, reg=0,
                  numb_iter=n_it, psf=psf)

    ans2 = egraal(gamma, lvl, v, weights, reg=0.5,
                  numb_iter=n_it, psf=psf)
    
    ans3 = egraal(gamma, lvl, v, weights, reg=-1,
                  numb_iter=n_it, psf=psf)

    np.save('{0}-full_ans.npy'.format(filename), ans1, ans2, ans3)
    x1, x2, x3 = ans1[1][0], ans2[1][0], ans3[1][0]
    succ_error1 = np.array(ans1[0])
    succ_error2 = np.array(ans2[0])
    succ_error3 = np.array(ans3[0])
    save_data(filename, x1, x2, x3, succ_error1, succ_error2,
              succ_error3, v)
    
    x1_ = np.clip(x1, 0, 1.4)
    x2_ = np.clip(x2, 0, 1.4)
    x3_ = np.clip(x3, 0, 1.4)
    
