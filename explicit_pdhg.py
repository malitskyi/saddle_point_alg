#!/usr/bin/env python
"""Implmentation of PDHG algorithm with dynamical steps for the
statistical multiresolution estimation for image reconstruction.

Here is the main variable is u = (x, y_0, y_1, ..., y_lvl), where x in
N x N array, y is the dual variable for the total variation term (or
other norms) and is N x N x 2 array and y_i is the dual variable that
corresponds to the i-th lever of resolution and it is a 1d array of
length (N-i+1)**2

"""


import numpy as np
import warnings
from numpy.polynomial import Polynomial as poly
import scipy.linalg as LA
import scipy.io as sio
import scipy.sparse as spr
import matplotlib.pyplot as plt
from sklearn.feature_extraction.image import extract_patches, extract_patches_2d
from skimage import data
from skimage import io
from skimage import img_as_float
from skimage.color import rgb2gray
from skimage import transform
from time import perf_counter

from scipy.signal import fftconvolve

from misc_functions import *
#from simulate_noise import define_gamma

__author__ = "Yura Malitsky"
__license__ = "MIT License"
__email__ = "y.malitskyi@math.uni-goettingen.de"
__status__ = "Development"


def prox_g_i_conj(y, rho, v_y, gamma):
    """Proximal operator of the conjugate of the indicator function of all
    sets | x_1 + ... + x_p - v_1 - ... - v_p | <= gamma

    Parameters
    ----------
    y: 2-dim array
    rho: a proximal paramter > 0
    v_y: array of the same shape as y. 
       The center of the balls.
    gamma: a positive number. 

    Returns
    -------
    An array of y shape.
    """    
    n = y.shape[0]
    z = np.zeros(y.shape)
    diff = y - rho * v_y
    a = rho * gamma
    mask1 = diff < - a
    mask2 = diff >   a
    z[mask1] = (diff + a)[mask1]
    z[mask2] = (diff - a)[mask2]
    return z
    

def prox_G_conj(Y, Rho, v_dict, gamma, reg):
    """ Proximal operator of g^*.

    Parameters
    ----------
    Y: object of lvl+1 arrays. 
        Dual variable.
    Rho: 1 x (lvl+1) array of positive numbers. 
        The stepsizes.
    v_dict: dictionary with keys 1,..., lvl
        v_dict[i] has the same shape as Y[i]
    gamma: (1 x lvl) array of positive numbers.
    reg: scalar -1 or from [0,1]. 
       -1 refers to Laplacian, 0 to TV, others -- to Huber function

    Returns
    -------
    An array of Y shape

    """    
    lvl = len(Y) - 1
    prox_y = choose_prox_y(reg)
    Y[0] = prox_y(Y[0], Rho[0])
    for i in range(1, lvl+1):
        v_y = v_dict[i]
        Y[i] = prox_g_i_conj(Y[i], Rho[i], v_y, gamma[i])
    return Y


def K(x, lvl, psf=None):
    """Computes Kx for the primal-dual method.

    When psf (point spread function) is None, it is just reformatting
    of x. When psf is presented, then first we compute convolution of
    x, and then do the same reformatting.
    
    Parameters
    ----------
    x: 2d array.
    lvl: positive integer
       Defines number of levels.
    psf: 2d array, None by default. 
       It is a kernel for convolution.
    Returns
    -------
    Object array of length lvl+1.

    """
    Y = np.empty(shape=lvl+1, dtype=object)
    Y[0] = grad(x)
    if psf is not None:
       x = convolution(x, psf)
    for i in range(1, lvl+1):
        kernel = np.ones((i,i))
        Y[i] = transform_x_into_y(x, kernel)
    return Y



def KT(Y, lvl, psf=None):
    """Computes K^T Y for the primal-dual method.
   
    Parameters
    ----------
    Y: object array of length lvl + 1.
    lvl: positive integer
       Defines number of levels.
    psf: 2d array, None by default. 
       It is a psf kernel for convolution.
    Returns
    -------
    2d array.

    """
    N = Y[0].shape[0]
    x = 0
    for i in range(1, lvl+1):
        kernel = np.ones((i,i))
        x += transform_y_into_x(Y[i], kernel)
    if psf is not None:
        x = convolution_T(x, psf)
        
    x += grad_T(Y[0])
    return x

def define_Y(x, N, lvl):
    Y = np.empty(shape=lvl+1, dtype=object)
    Y[0] = np.zeros((N, N, 2))
    for i in range(1, lvl+1):
        Y[i] = np.zeros((N - i + 1, N - i + 1))
    return Y




def check_dot_products(N, lvl):
    """
    This function checks identity (x, KY) = (KTx, Y)
    """
    x = np.random.randn(N*N).reshape(N,N)
    x2 = np.random.randn(N*N).reshape(N,N)
    w = np.ones(lvl+1)
    Y = K(x2, lvl)
    #print(Y)
    Kx = K(x,lvl)
    q1 = np.vdot(x, KT(Y, lvl))
    q11 = np.vdot(x, KT(Y, lvl))
    q2 = dot_object(Kx, Y, w)
    print("(x, KTY)", q1, q11)
    print("(Kx, Y)", q2)
    if q1 != q2:
        print("----checking TV-----")
        y = grad(x2)
        Ax = grad(x)
        print("(x, ATy)", np.vdot(x, grad_T(y)))
        print("(Ax, y)", np.vdot(Ax, y))

        for i in range(1, lvl+1):
            print("checking level", i)
            kernel = np.ones((i,i))
            y = transform_x_into_y(x2, kernel)
            Ax = transform_x_into_y(x, kernel)
            x22 = transform_y_into_x(y, kernel)
            print("(Ax, y)", np.vdot(Ax, y))
            print("(x, Ay)", np.vdot(x, x22))

def find_theta(u, v, u_old, v_old, theta_old, alpha_old, tau, sigma):
    """Find explicitly theta for the PDHG algorithm. 

    This is an auxiliary function for the the primal-dual
    method. Instead of running an expensive linesearch for a problem
    min g(x) s.t. Ax = b it computes stepsize very efficiently. Given
    all necessary vectors, it computes few dot products and norms
    which is not expensive and then solves 4-th degree polynomial
    inequality by a simple linesearch.

    """
    uu = u - u_old
    vv = v - v_old

    c = alpha_old**2 * tau

    a = np.zeros(5)
    a[0] = - np.vdot(u, u)
    a[1] = -2 * np.vdot(u, uu)
    a[2] = c * norm_square_object(v, sigma) - np.vdot(uu, uu)
    a[3] = 2*c * dot_object(v, vv, sigma)
    a[4] = c * norm_square_object(vv, sigma)
    p = poly(a)
    th = np.sqrt(1 + theta_old)

    for i in range(100):
        if p(th) > 0:
            th *= 0.9
        else:
            break

    return th
            
def explicit_pdhg(gamma, lvl, x0, tau, sigma,
                  reg=0, numb_iter=100, psf=None):
    """Implementation of the explcit primal-dual algorithm for solving
    SMRE in the form of min_x g_0(A_0x) + \dots + g_m(A_mx).

    Here g_0 is |.|_{2,1} norm, A_0 is the gradient of the image; g_s
    = Sum_{i\in s} (y_i - x0_i)^2 A_s = P_s A, where P_s is a
    projection onto square s and A is the forward operator (identity
    or convolution)

    The explcit primal-dual algorithm is from [1], its main feature is
    that it does not require to know ||A_i||.
    
    Parameters 
    ---------- 
    gamma is an 1 x lvl array of positive numbers. 
       Measures disrepancy on different resolutions between Ax
       and x0
    lvl: positive integer
       number of levels in multiscaling approach 
    tau: positive number
    sigma: 1 x (lvl+1) array of positive real weights.
    reg: -1 or other value from [0,1]. 
       -1 refers to Laplacian, 0 to TV, others -- to Huber functiono
    numb_iter: positive integer
       Number of iterations.
    psf: 2d array.
       Kernel for a convolution.

    Returns
    -------
    optim_gap: list of nonnegative numbers.
             Optimality gaps, computed in every iteration.
    feas_gap: list of nonnegative numbers.
             Feasibility gaps, computed in every iteration.
    x: 2d array.
      Reconstructed image.
    Y: 1d object array.
      Dual solutions.
    succ_error": list of nonnegative numbers.
      Collects all residuals |u^k-u^{k-1}|


    References
    ----------
    [1]: Malitsky Y, Pock T. A first-order primal-dual algorithm with
    linesearch. SIAM Journal on Optimization. 2018, 28(1):411-32.

    """
    begin = perf_counter()
    N = x0.shape[0]
    x = x0.copy()
    v_dict = make_v_dict(x0, lvl)
    Kx = K(x, lvl, psf)
    Y = define_Y(x, N, lvl)
    alpha_old = np.sqrt(np.vdot(x,x)/(tau*norm_square_object(Kx, sigma)))
    feas_gap = []
    optim_gap = []
    succ_error = []
    u_old = 0
    v_old = 0
    theta_old = 1

    for i in range(numb_iter):
        Y1 = prox_G_conj(Y + sigma * alpha_old * Kx, sigma*alpha_old, v_dict, gamma, reg)
        u = KT(Y1, lvl, psf)
        v = K(u, lvl, psf)
        theta = find_theta(u, v, u_old, v_old, theta_old, alpha_old, tau, sigma)
        alpha = alpha_old * theta
        x_incr = alpha * tau * ((1+theta)*u - theta*u_old)
        x -=  x_incr
        Kx -=  alpha * tau * ((1+theta)*v - theta*v_old)
        residual = np.sqrt(LA.norm(x_incr)**2 + norm_square_object(Y1 - Y))
        feas_gap.append(LA.norm(u))
        optim_gap.append(np.sqrt(norm_square_object(Y1 - Y, 1/sigma)) / (alpha_old))
        succ_error.append(residual)
        u_old, v_old = u, v
        theta_old = theta
        alpha_old = alpha
        Y = Y1
        
    end = perf_counter()
    print("Explicit PDHG, time execution:", end-begin)
    return optim_gap, feas_gap, x, Y, succ_error

