#!/usr/bin/env python
"""
Auxiliary function that plot and save data in various formats

"""


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from misc_functions import *

mpl.rc('lines', linewidth=2)
mpl.rcParams.update(
    {'font.size': 12, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})
mpl.rcParams['xtick.major.pad'] = 2
mpl.rcParams['ytick.major.pad'] = 2

def save_data(filename, x1, x2, x3, succ_error1, succ_error2,
              succ_error3, v,  epochs=False):
    """
    Save all data to text, binary files. Plot images and save them
    also to png and eps formats, output results

    """
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4,4))
    ax.plot(succ_error1, 'b', label='TV')
    ax.plot(succ_error2, 'g', label='Huber')
    ax.plot(succ_error3, 'r', label='Laplace')
    ax.set_yscale('log')
    ax.set_ylabel('residual')
    if epochs:
        ax.set_xlabel('epochs')
    else:
        ax.set_xlabel('iterations')
    ax.legend()
    plt.savefig('{0}-residual_plots.png'.format(filename), bbox_inches='tight')
    plt.savefig('{0}-residual_plots.eps'.format(filename), bbox_inches='tight')
    plt.savefig('{0}-residual_plots.pdf'.format(filename), bbox_inches='tight')

    plt.show()
    plt.clf()

    np.savetxt('{0}-residual_TV.txt'.format(filename), succ_error1)
    succ_error1.tofile('{0}-residual_TV.dat'.format(filename))
    
    np.savetxt('{0}-residual_Huber.txt'.format(filename), succ_error2)
    succ_error2.tofile('{0}-residual_Huber.dat'.format(filename))

    np.savetxt('{0}-residual_Laplace.txt'.format(filename), succ_error3)
    succ_error3.tofile('{0}-residual_Laplace.dat'.format(filename))
    
    print("Energy TV:  original {0:.0f}, final {1:.0f}".format(TV(v), TV(x1)))
    print("Energy Huber-0.5: original {0:.0f}, final {1:.0f}".format(Huber(v, 0.5), Huber(x2, 0.5)))
    print("Energy Laplace: original {0:.0f}, final {1:.0f}".format(Laplacian(v), Laplacian(x3)))
    fig, ax = plt.subplots(nrows=1, ncols=4, figsize=(20,4))
    ax[0].imshow(v, cmap='gray')
    ax[1].imshow(x1, cmap='gray')
    ax[2].imshow(x2, cmap='gray')
    ax[3].imshow(x3, cmap='gray')
    #ax[2].plot(succ_error2, 'b')
    #ax[2].set_yscale('log')
    
    plt.imsave('{0}-image_TV.png'.format(filename), x1, cmap='gray')
    plt.imsave('{0}-image_TV.eps'.format(filename), x1, cmap='gray')

    plt.imsave('{0}-image_Huber.png'.format(filename), x2, cmap='gray')
    plt.imsave('{0}-image_Huber.eps'.format(filename), x2, cmap='gray')

    plt.imsave('{0}-image_Laplace.png'.format(filename), x3, cmap='gray')
    plt.imsave('{0}-image_Laplace.eps'.format(filename), x3, cmap='gray')

